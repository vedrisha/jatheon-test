import Vue from 'vue'
import { MultiSelectPlugin } from '@syncfusion/ej2-vue-dropdowns';
import { MultiSelect, CheckBoxSelection } from '@syncfusion/ej2-dropdowns';

MultiSelect.Inject(CheckBoxSelection);
Vue.use(MultiSelectPlugin);

